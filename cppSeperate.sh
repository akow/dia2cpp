#!/bin/bash

usage="$(basename "$0") <OPTIONS> <root> -- Program to generate cpp classes code from a dia diagram\n
\n
where:\n
\troot\t\tdirectory\n"

OPTIND=1
DIRECTORY=.
USE_NAMESPACE_DIRS=false

while getopts "" opt; do
  case $opt in
    \?)
      echo -e $usage
			exit
      ;;
  esac
done


shift $(($OPTIND - 1))
if [ ! -d "$1" ]; then
	echo -e $usage
	exit
fi


ROOT=`realpath $1`
ROOT_ABOVE=`echo ${ROOT%/*}`
ROOT_BASENAME=`basename $ROOT`


DEST_DIR="${ROOT_BASENAME}_project"
if [ -d $DEST_DIR ];then
	echo -e "FATAL: Folder $DEST_DIR already exists!"
	exit
else
	mkdir -p $DEST_DIR
fi

INC_DIR="${DEST_DIR}/include"
SRC_DIR="${DEST_DIR}/src"
mkdir -p $INC_DIR
mkdir -p $SRC_DIR

HEADERS=`ls -R $ROOT_BASENAME | awk '
/:$/&&f{s=$0;f=0}
/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}
NF&&f{ print s"/"$0 }' | grep .hpp`

SRC=`ls -R $ROOT_BASENAME | awk '
/:$/&&f{s=$0;f=0}
/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}
NF&&f{ print s"/"$0 }' | grep .cpp`

for s in $SRC;do
	cp --parents $s $SRC_DIR
	echo "cp --parents $s $SRC_DIR"
done

for h in $HEADERS;do
	# Finally, copy the header over
	cp --parents $h $INC_DIR
	echo "cp --parents $h $INC_DIR"
done 

echo -n "Correcting headers..."

for h in $HEADERS;do
	# Correct #include in srcs
	from=`basename $h`
	full=`realpath $h`
	to=`echo ${full#"$ROOT_ABOVE/"}`
	find $SRC_DIR -type f -print0 | xargs -0 sed -i "s@$from@$to@g"
	find $INC_DIR -type f -print0 | xargs -0 sed -i "s@$from@$to@g"
done

echo "done. Output directory: $DEST_DIR"
























