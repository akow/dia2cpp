#!/bin/bash
 

usage="$(basename "$0") <OPTIONS> <diafile> -- Program to generate cpp classes code from a dia diagram\n
\n
where:\n
\t-d\t\t<VAR> Destination directory\n
\t-n\t\tUse namespace directories
\tdiafile\t\tfile\n"

OPTIND=1
DIRECTORY=.
USE_NAMESPACE_DIRS=false

while getopts ":d:n" opt; do
  case $opt in
		n) 
			USE_NAMESPACE_DIRS=true # Sort code into folders by namespace
			;;
    d)
			DIRECTORY=$OPTARG
			if [ ! -d "$DIRECTORY" ]; then
				echo "Directory $DIRECTORY does not exist!"
				echo -e $usage
				exit 1
			fi
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
			echo -e $usage
      ;;
  esac
done


shift $(($OPTIND - 1))
DIA_FILE=$1

if [ ! $DIA_FILE ]; then
	echo 	"Require a dia file"
	echo -e $usage
	exit 1
fi

command -v dia2code >/dev/null 2>&1 || {
	echo "I require dia2code but it's not installed.  Aborting." >&2; exit 1; 
}

dorig=$DIRECTORY/orig
if [ -d $dorig ]
then
	rm -r $dorig
fi
mkdir $dorig

dtmp=$DIRECTORY/tmp
if [ -d $dtmp ]
then
	rm -r $dtmp
fi
mkdir $dtmp

dsame_names=$DIRECTORY/dia2cpp_samenames
if [ -d $dsame_names ]
then
	rm -r $dsame_names
fi
mkdir $dsame_names

dia2code -t cpp -d $dorig $DIA_FILE

filename=`ls $dorig`
filename=$dorig/$filename

classes=`grep "/// class" $filename  | awk -v N=3 '{print $N}'`

declare -a same_names

for c in $classes
do
	dia2code -t cpp -d $dtmp $DIA_FILE -cl $c

	dest_fp=$DIRECTORY/${c}.hpp
	source_fp=$dtmp/`ls $dtmp`
	mv $source_fp $dest_fp


	# dia2code doesnt isolate classes, even with -cl. We need to clean this up
	local_classes=`grep "/// class" $dest_fp  | awk -v N=3 '{print $N}'`
	for lc in $local_classes
	do
		if [ "$lc" != "$c" ]
		then
			# Include this
			echo -e "#include \"${lc}.hpp\"\n`cat $dest_fp`" > $dest_fp
			
			# Remove class
			line_no=`awk "/\/\/\/ class $lc/{ print NR; exit }" $dest_fp`
			sed "$line_no,/^$/d" "$dest_fp" -i
		fi
	done

	# dia2code returns multiple classes with the same name but in different
	#  namespaces. We put them into dia2cpp_samenames folder
	n_classes=`grep "/// class $c" $dest_fp | wc -l`
	if [ $n_classes -gt 1 ]
	then
		mv $dest_fp $dsame_names
		same_names+=("$c")
	else

		# Remove empty namespaces
		tot_ns=`grep "namespace" $dest_fp | wc -l`
		for i in `seq 1 $tot_ns`
		do
			sed -e ':a;N;$!ba;s/namespace \w* {[\n\t ]*};//g' $dest_fp -i
		done

		# Get namespaces
		ns=`grep "namespace" $dest_fp  | awk -v N=2 '{print $N}'`

		# Remove empty lines between namespaces 
		sed -e ':a;N;$!ba;s/{[\n]*[\t ]*namespace/{\nnamespace/g' $dest_fp -i

		# Append namespace naming at end
		sed -e ':a;N;$!ba;s/};[};\n\t ]*/};/g' $dest_fp -i
		IFS=' ' read -a arr_ns <<< `echo $ns`
		
		for (( idx=${#arr_ns[@]}-1 ; idx>=0 ; idx-- ))
		do
			echo -e "}; // namespace ${arr_ns[idx]}"  >> $dest_fp
		done


		# Create cpp for non template classes
		class_dec_line_no=`awk "/\/\/\/ class $c/{ print NR; exit }" $dest_fp`
		let exp_temp_line_no=$class_dec_line_no+1
		TEMP=`sed "${exp_temp_line_no}q;d" $dest_fp | grep template`
		if [ -z "$TEMP" ]
		then
			cpp_fp=$DIRECTORY/${c}.cpp
			echo "#include\"${c}.hpp\"" >> $cpp_fp
			i=0
			for n in $ns
			do
				echo -e "namespace $n {\n\t" >> $cpp_fp
				((i++))
			done

			for (( idx=${#arr_ns[@]}-1 ; idx>=0 ; idx-- ))
			do
				echo -e "} // namespace ${arr_ns[idx]}"  >> $cpp_fp
			done


			#for n in $ns
			#do
		#			for j in `seq 1 $i`
	#				do
#						echo -e "\t" >> $cpp_fp
#					done
#					echo -e "};\n" >> $cpp_fp
#			done
		fi
				

		# add #define guards
		guard="__"
		for n in $ns
		do
			guard=$guard$n"_"
		done
		guard=$guard$c"__"
		guard=`echo $guard | awk '{print toupper($0)}'`
		echo -e "#ifndef $guard\n#define $guard\n\n`cat $dest_fp`" > $dest_fp
		echo -e "\n\n#endif" >> $dest_fp



		if [ "$USE_NAMESPACE_DIRS" = true ]
		then
			# Restructur eby namespace
			# Build the namespace path
			dir=$DIRECTORY
			for n in $ns
			do
				dir=$dir/$n
			done
			
			mkdir -p $dir
			mv ${DIRECTORY}/${c}.*pp $dir	

			# print to stdout location
			echo $dir/${c}.hpp
			if [ -f $dir/${c}.cpp ]
			then
				echo $dir/${c}.cpp
			fi
		else
			echo $DIRECTORY/${c}.hpp
			if [ -f $DIRECTORY/${c}.cpp ]
			then
				echo $DIRECTORY/${c}.cpp
			fi
		fi
	fi
done

if [ ${#same_names[@]} -ne 0 ]
then
	echo -e "${#same_names[@]} classes contained multiple class definitions. Possible causes:\n\tSame name different namespace.\tAlt: Use different names"

	# Get unique
	echo "---------------------"
	ls $dsame_names
	echo -e "---------------------"
else
	rm -r $dsame_names
fi


#cleanup
rm -r $dtmp
rm -r $dorig




